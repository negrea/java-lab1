package co.zipperstudios.repo;

import co.zipperstudios.interfaces.RepoElement;
import co.zipperstudios.interfaces.Repo;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * Created by Vlad on 07/10/2016.
 */

public class CustomRepository implements Repo {
    RepoElement[] array;
    int mSize = 0;

    public CustomRepository(int size) {
        array = new RepoElement[size];
    }

    @Override
    public void add(RepoElement item) {
        array[mSize++] = item;
    }

    @Override
    public void remove(int index) {
        for (int i = index; i < mSize - 1; i++) {
            array[i] = array[i + 1];
        }

        mSize --;
    }

    @Override
    public List<RepoElement> getAllElements() {
        ArrayList<RepoElement> allElements = new ArrayList<RepoElement>();
        allElements.addAll(Arrays.asList(array).subList(0, mSize));
        
        return allElements;
    }
}
