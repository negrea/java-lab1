package co.zipperstudios.view;

import co.zipperstudios.controller.Controller;
import co.zipperstudios.exceptions.BillableException;
import co.zipperstudios.interfaces.RepoElement;
import co.zipperstudios.model.Car;
import co.zipperstudios.model.Motorcycle;
import co.zipperstudios.model.Truck;

/**
 * Created by Vlad on 13/10/2016.
 */
public class View {
    Controller mController;

    public View(Controller controller) {
        mController = controller;

        init();
    }

    private void init() {
        Car car1 = new Car();
        car1.setPrice(2000);
        mController.add(car1);

        Car car2 = new Car();
        car2.setPrice(989);
        mController.add(car2);

        Truck truck = new Truck();
        truck.setPrice(1249);
        mController.add(truck);

        Motorcycle motorcycle = new Motorcycle();
        motorcycle.setPrice(200);
        mController.add(motorcycle);

        System.out.println("All elements");
        for (RepoElement element : mController.getAllElements()) {
            System.out.println(element);
        }

        System.out.println("\nElements with cost greater than 1000");
        for (RepoElement element : mController.getAllBillablesWithCostBigger(1000)) {
            System.out.println(element);
        }

        System.out.println("\nRemoved second car");
        mController.remove(1);

        System.out.println("\nAll elements");
        for (RepoElement element : mController.getAllElements()) {
            System.out.println(element);
        }

        System.out.println("\nElements with cost greater than 1000");
        for (RepoElement element : mController.getAllBillablesWithCostBigger(1000)) {
            System.out.println(element);
        }

        try {
            Car car3 = new Car();
            car3.setPrice(-100);
        } catch (BillableException e) {
            System.out.print("Caught exception: " + e.getMessage() + '\n');
        }
    }
}
