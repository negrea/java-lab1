package co.zipperstudios.model;

import co.zipperstudios.exceptions.BillableException;

/**
 * Created by Vlad on 13/10/2016.
 */
public abstract class BaseBillable {
    protected double mPrice;

    public double getPrice() {
        return mPrice;
    }

    public void setPrice(double price) {
        if (price < 0) {
            throw new BillableException();
        }

        this.mPrice = price;
    }
}
