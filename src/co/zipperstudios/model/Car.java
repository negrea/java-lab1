package co.zipperstudios.model;

import co.zipperstudios.interfaces.RepoElement;

/**
 * Created by Vlad on 13/10/2016.
 */
public class Car extends BaseBillable implements RepoElement {
    @Override
    public String toString() {
        return "Car with the cost: " + mPrice;
    }
}
