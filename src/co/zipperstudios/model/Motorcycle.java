package co.zipperstudios.model;

import co.zipperstudios.interfaces.RepoElement;

/**
 * Created by Vlad on 13/10/2016.
 */
public class Motorcycle extends BaseBillable implements RepoElement {
    @Override
    public String toString() {
        return "Motorcycle with the cost: " + mPrice;
    }
}
