package co.zipperstudios.exceptions;

/**
 * Created by Vlad on 14/10/2016.
 */
public class BillableException extends RuntimeException {
    public BillableException() {
        super("Cost cannot be negative");
    }

    public BillableException(String message, Throwable cause) {
        super("Cost cannot be negative", cause);
    }
}
