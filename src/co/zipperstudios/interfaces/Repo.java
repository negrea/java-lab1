package co.zipperstudios.interfaces;

import java.util.List;

/**
 * Created by Vlad on 07/10/2016.
 */
public interface Repo {
    void add(RepoElement item);

    void remove(int index);

    List<RepoElement> getAllElements();
}
