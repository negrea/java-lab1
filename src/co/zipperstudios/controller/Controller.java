package co.zipperstudios.controller;

import co.zipperstudios.interfaces.Repo;
import co.zipperstudios.interfaces.RepoElement;
import co.zipperstudios.model.BaseBillable;
import co.zipperstudios.repo.CustomRepository;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Vlad on 07/10/2016.
 */
public class Controller {
    Repo mRepository;

    public Controller() {
        mRepository = new CustomRepository(20);
    }

    public void add(RepoElement item) {
        mRepository.add(item);
    }

    public void remove(int index) {
        mRepository.remove(index);
    }

    public List<RepoElement> getAllElements() {
        return mRepository.getAllElements();
    }

    public List<RepoElement> getAllBillablesWithCostBigger(double minCost) {
        ArrayList<RepoElement> results = new ArrayList<RepoElement>();

        for (RepoElement element : mRepository.getAllElements()) {
            BaseBillable billable = (BaseBillable) element;
            if (billable.getPrice() > minCost) {
                results.add(element);
            }
        }

        return results;
    }
}
