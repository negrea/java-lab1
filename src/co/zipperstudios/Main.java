package co.zipperstudios;

import co.zipperstudios.controller.Controller;
import co.zipperstudios.view.View;

public class Main {

    public static void main(String[] args) {
        Controller controller = new Controller();
        new View(controller);
    }
}
